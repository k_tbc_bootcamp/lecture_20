package com.lkakulia.lecture_20.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import com.lkakulia.lecture_20.R
import com.lkakulia.lecture_20.http_request.CustomCallback
import com.lkakulia.lecture_20.http_request.HttpRequest
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()
    }

    private fun init() {
        loginButton.setOnClickListener({
            generalListener(HttpRequest.LOGIN)
        })

        registerButton.setOnClickListener({
            generalListener(HttpRequest.REGISTER)
        })
    }

    private fun parseJson(response: String) {
        val json = JSONObject(response)

        if (json.has("token")) {
            val token = json.getString("token")
            val email = emailEditText.text.toString()

            val intent = Intent(this, UsersActivity::class.java)
            intent.putExtra("email", email)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
        else if (json.has("error")) {
            val error = json.getString("error")
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
        }
    }

    private fun generalListener(path: String) {
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty()) {
            val map = mutableMapOf<String, String>()
            map.put("email", email)
            map.put("password", password)

            HttpRequest.postRequest(path, map, object : CustomCallback {
                override fun onFailure(error: String) {
                    d("onFailure", " ${error}")
                }

                override fun onResponse(response: String) {
                    parseJson(response)
                }
            })
        }

        else {
            Toast.makeText(this, "Please fill in all the fields", Toast.LENGTH_SHORT).show()
        }
    }
}
