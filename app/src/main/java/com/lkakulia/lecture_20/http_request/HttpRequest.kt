package com.lkakulia.lecture_20.http_request

import android.util.Log.d
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*

object HttpRequest {

    const val LOGIN = "login"
    const val REGISTER = "register"
    const val URL = "https://reqres.in/api/"

    private var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl(URL)
        .build()

    private var service = retrofit.create(ApiService::class.java)

    fun getRequest(path: String, callback: CustomCallback) {
        val call = service.getRequest(path)
        call.enqueue(onCallBack(callback))
    }

    fun postRequest(path: String, parameters: MutableMap<String, String>, callback: CustomCallback) {
        val call = service.postRequest(path, parameters)
        call.enqueue(onCallBack(callback))
    }

    private fun onCallBack(callback: CustomCallback) = object: Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            callback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            if (response.isSuccessful) {
                d("loggedIn", " ${response.body()}")
                callback.onResponse(response.body().toString())
            }
            else {
                val json = JSONObject(response.errorBody()!!.string())

                if (json.has("error")) {
                    callback.onResponse(json.toString())
                }
            }
        }
    }

    interface ApiService {
        @GET("{path}")
        fun getRequest(@Path("path") path: String): Call<String>

        @FormUrlEncoded
        @POST("{path}")
        fun postRequest(@Path("path") path: String, @FieldMap parameters: MutableMap<String, String>): Call<String>
    }
}