package com.lkakulia.lecture_20.http_request

interface CustomCallback {
    fun onFailure(error: String)
    fun onResponse(response: String)
}